$(document).ready( function () {
    console.log('map.js ready!');

    map = new ol.Map({
        target: 'map-element',
        layers: [
    		new ol.layer.Tile({
    			source : new ol.source.TileJSON({
    				url: 'https://api.tiles.mapbox.com/v3/qianyanseu.jomlod6d.jsonp?access_token=pk.eyJ1IjoicWlhbnlhbnNldSIsImEiOiJsNnE1YVhrIn0.Tf_DvFH4ZP26QflrFxSBmA',
    				crossOrigin: 'anonymous'
    			})
    		})
        ],
        view: new ol.View({
            center: [13528852.70518923, 3663618.14253868],
            zoom: 10
        })
    });

    // var bbox source =
    bboxLayer = new ol.layer.Vector({
        source: new ol.source.GeoJSON({
            projection: 'EPSG:3857',
            url: 'resources/data/bbox_line.geojson'
        }),
        style: new ol.style.Style({
            fill: new ol.style.Fill({
                color: 'rgba(255, 255, 255, 0.3)'
            }),
            stroke: new ol.style.Stroke({
                color: '#d95f0e',
                width: 2
            })
        })
    });
    map.addLayer(bboxLayer);

    var contourSource = new ol.source.ServerVector({
	  format: new ol.format.GeoJSON(),
	  loader: function(extent, resolution, projection) {
	    var url = 'http://www.thegisguy.tk:8080/geoserver/wfs?service=WFS&' +
        'version=1.1.0&request=GetFeature&typename=pgrouting:contour_3857&' +
        'outputFormat=text/javascript&format_options=callback:loadFeatures' +
        '&srsname=EPSG:3857&bbox=' + extent.join(',') + ',EPSG:3857';
	    $.ajax({
	      url: url,
	      dataType: 'jsonp'
	    });
	  },
	  strategy: ol.loadingstrategy.createTile(new ol.tilegrid.XYZ({
	    maxZoom: 19
	  })),
	  projection: 'EPSG:3857'
	});

	loadFeatures = function(response) {
	  contourSource.addFeatures(contourSource.readFeatures(response));
	};

	var contourLayer = new ol.layer.Vector({
	  source: contourSource,
	  style: new ol.style.Style({
	    stroke: new ol.style.Stroke({
	      color: 'rgba(0, 255, 0, 1.0)',
	      width: 2
	    })
	  })
	});
	// map.addLayer(contourLayer);

	var params = {
	  LAYERS: 'pgrouting:contour_3857',
	  FORMAT: 'image/png',
	  TILED: 'true'
	};

	var contourWMSLayer = new ol.layer.Image({
      source: new ol.source.ImageWMS({
        url: 'http://thegisguy.tk:8080/geoserver/pgrouting/wms',
        params: params
      })
    });
    map.addLayer(contourWMSLayer);


    transform = ol.proj.getTransform('EPSG:3857', 'EPSG:4326');

});
