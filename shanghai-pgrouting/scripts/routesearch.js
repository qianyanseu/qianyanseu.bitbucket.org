routeSearchMode = false;
source = '';
destination = '';

function initRouteSearchMode() {
	if (routeSearchMode == true) {
		return;
	}
	routeSearchMode = true;

	if ($('#route_search_prompt').length == 0) {
		createRouteSearchPrompt();
	}


}

function createRouteSearchPrompt() {
		// 在搜索框下方显示的结果列表
    var html = '<div id="route_search_prompt" class="search_prompt col-md-2">';
    html += '<h4>换乘信息</h4>';
    html += '<p id="SourceName">起点' + source + '</p>';
    html += '<p id="DestinationName">终点：>' + destination + '</p>';
    html += '<div id="route"><ol>';
	html += '<p>路线总长度约为 2.2km</p>';
	html += '<li>步行至华西门站 230米</li>';
	html += '<li>乘坐707路，在南院门下车 3站</li>';
	html += '<li>步行至biangbiang面（总店） 110m </li>';
	html += '</ol></div>';
	html += '<a href="#" id="execute_route_search"> 路径搜索 </a>';
    html += '<a href="#" id="back_to_map"> 返回地图 </a>';
    html += '</div>';

    /*
    <div id="search_prompt" class="search_prompt col-md-2">
        <div class="list-group col-md-12">
            <a href="#" class="list-group-item active">Cras justo odio</a>
            <a href="#" class="list-group-item">Dapibus ac facilisis in</a>
            <a href="#" class="list-group-item">Morbi leo risus</a>
            <a href="#" class="list-group-item">Porta ac consectetur ac</a>
            <a href="#" class="list-group-item">Vestibulum at eros</a>
        </div>
    </div>
    */

    /*
    <div id="search_prompt" class="search_prompt col-md-2">
        <div class="list-group col-md-12">
            <a href="#" class="list-group-item active">与sss 有关的第1条结果</a>
            <a href="#" class="list-group-item active">与sss 有关的第2条结果</a>
            <a href="#" class="list-group-item active">与sss 有关的第3条结果</a>
            <a href="#" class="list-group-item active">与sss 有关的第4条结果</a>
            <a href="#" class="list-group-item active">与sss 有关的第5条结果</a>
        </div>
    </div>
    */
    $(html).insertAfter($('#map_header')).fadeIn('slow');

    $('#execute_route_search').click(function(event) {
    	event.preventDefault();
    	route_search();
    });

    $('#back_to_map').click( function (event) {
    	event.preventDefault();
    	$('#route_search_prompt').remove();
    	clearRouteSearchLayer();
    });

    $('#route').hide();
}

function clearRouteSearchLayer() {
	if (typeof placeSearchLayer != 'undefined') {
		routeSearchLayer.getSource().clear();
		// placeSearchLayer.source = null;
	}

	routeLayer.set('visible', false);

    // showVectorLayers();
    // showingSearchResults = false;
}

function setRouteSource (feature_name) {
	source = feature_name;
	initRouteSearchMode();
	$('#SourceName').text('起点：' + feature_name);
}

function setRouteDestination (feature_name) {
	destination = feature_name;
	initRouteSearchMode();
	$('#DestinationName').text('终点：' + feature_name);
}

function route_search() {
	var url = '/static/silkroad/data/' + source + '-' + destination + '.geojson';
	route_search_url(url);
}

function route_search_url(url) {
	if (url == '/static/silkroad/data/布丁酒店连锁（西安钟楼北大街店）-biangbiang面(总店).geojson')
	{
		routeLayer.set('visible', true);
		$('#route').show();


		$('#execute_route_search').insertBefore(html);

	}
	else {
		search_alert('获取换乘信息失败');
	}
}

function handle_route_result_url(url) {


    routeSearchOverlay = new ol.FeatureOverlay({
      map: map,
      style: function(feature, resolution) {
        var text = feature.get('name');

        var style = [new ol.style.Style({
            stroke: new ol.style.Stroke({
              // color: '#00f',
              // color: '#DAA520',
              color: '#319FD3',
              width: 1
            }),
            fill: new ol.style.Fill({
              color: 'rgba(255,255,255,0.6)'
            }),
            text: new ol.style.Text({
              font: 'Normal 16px Arial;;',
              text: text,
              fill: new ol.style.Fill({
                color: '#000'
              }),
              stroke: new ol.style.Stroke({
                // color: '#f00',
                // width: 3
                color: '#fff',
                width: 3
              })
            })

        })];
        return style;
      }
    });


    var iconStyle = new ol.style.Style({
      image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
        anchor: [0.5, 46],
        anchorXUnits: 'fraction',
        anchorYUnits: 'pixels',
        opacity: 0.75,
        src: '/static/silkroad/img/map_marker 32x32.png'
      }))
    });


    /*
    $('#btn-group-cities').children('button').click(function () {
        var cityName = $(this).attr('id');
        if (lonlat = cityPositions[cityName]) {
            var coordinate = ol.proj.transform(lonlat, 'EPSG:4326', 'EPSG:3857');
            var pan = ol.animation.pan({
                duration: 500,
                source: (map.getView().getCenter())
            });
            map.beforeRender(pan);
            map.getView().setCenter(coordinate);
        }
    });

    */
    // var json = JSON.parse(data);

    /*
    // 判断是否返回了有效的搜索结果
    if (data.status == 'ZERO_RESULTS')
    {
        search_alert('找不到结果，请修改关键词重试');
        return;
    }
    */

    /*
    var vectorSource = new ol.source.Vector({
        features: result_features
    });
	*/



    routeSearchLayer = new ol.layer.Vector({
        source: new ol.source.GeoJSON({
            projection: 'EPSG:3857',
            url: url
        }),
        style: new ol.style.Style({
        	stroke: new ol.style.Stroke({
	    		color: '#319FD3',
	          	width: 2,
	          	lineDash: 4
        	})
        })
        /*function(feature, resolution) {
        // var text = feature.get('name');


        var style = [new ol.style.Style({
            stroke: function () {
            	if (feature.get('type') == '步行')
            	{
	            	return new ol.style.Stroke({
		              // color: '#00f',
		              // color: '#DAA520',
		              color: '#319FD3',
		              width: 2,
		              lineDash: 4
		            });
	            }
	            else {
	            	return new ol.style.Stroke({
		              // color: '#00f',
		              // color: '#DAA520',
		              color: '#319FD3',
		              width: 3
		            });
	            }}
        })];

        return style;
      }
      */
    });


    if (routeSearchLayer.getSource().getFeatures().length == 0)
    {
    	//search_alert('获取换乘信息失败');
    }
    else {
    	map.addLayer(routeSearchLayer);
    }
    map.addLayer(routeSearchLayer);




    /*
    // 定位到第一条结果
    if (map.getView().getZoom() < 10) {
        map.getView().setZoom(10);
    }

    var destination = result_features[0];
    // var lonlat = [destination.getProperties()['lat'], destination.getProperties()['lon']];
    // var lonlat = destination.getProperties()['geometry']
    // var coordinate = ol.proj.transform(lonlat, 'EPSG:4326', 'EPSG:3857');
    var coordinate = destination.getGeometry()['j'];

    var pan = ol.animation.pan({
            duration: 500,
            source:  (map.getView().getCenter())
        });
    map.beforeRender(pan);
    map.getView().setCenter(coordinate);
    */

}

$(document).ready(function() {
});