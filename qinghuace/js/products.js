/**
 * Created by Yan on 2015/2/2.
 */
$(document).ready(function() {

    // var $container = $('#products');

    var width = document.documentElement.clientWdith;

    var $container = $('#products');

    $container.imagesLoaded(function() {



        $('#products').removeClass('am-hide').masonry({
            columnWidth: '.msry-item', // '.msry-item',
            itemSelector: '.msry-item',
            isAnimated: false,
            isFitWidth: true
        });

        $('.msry-item').each(function (index, value) {
            $(this).css("-webkit-animation-delay", index * 0.3 + 's');
            $(this).addClass('am-animation-scale-up');
        });

        $('#nav-item-index').removeClass('am-hide').addClass('am-animation-slide-left');
        $('#nav-item-call').removeClass('am-hide').addClass('am-animation-slide-right');



    });
});

