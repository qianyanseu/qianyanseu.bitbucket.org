/**
 * Created by Yan on 5/7/2015.
 */

var loadedCount = 0;

var imgLoaded = function(){
    loadedCount ++;
    var imgAll = $("body img");
    var proBarLength = $("#loadProgBar").width();
    $("#leftProgBar").css("width", (loadedCount / imgAll.length * proBarLength) + "px");
    if(loadedCount == imgAll.length){
        //全部图片加载完成
        $("#fullpage").fadeIn();
        $("#loadProgBar").fadeOut();
        $("body").addClass("fadeInUp");
    }
};
$(document).ready(
    function(){
        var imgAll = $("body img");
        var imgCount = imgAll.length;
        for(var i = 0; i < imgCount; i++){
            var curImg = new Image();
            curImg.onload = imgLoaded;
            curImg.src = imgAll[i].src;
        }
    }
);

var loadFirstPageStructure = function(){
    var nextIndex =1 ;
    var coverTopWidth = $("#section" + nextIndex + " .cover-top").width();
    var covTopImg = $("#section" + nextIndex + " .cover-top img")[0];
    var tmpImg = new Image();
    tmpImg.src = covTopImg.src;
    //按头的比例设置上部分高度
    var coverTop = $("#section" + nextIndex + " .cover-top").css("height", tmpImg.height * coverTopWidth / tmpImg.width + "px");
    $("#section" + nextIndex + " .cover-top img").css("width","100%").css("height", "100%");
    var coverSecHeight =  $("#section" + nextIndex).height();
    var coverCentHeight = $(document).height() * 0.97 - coverTop.height();
    var coverCent = $("#section" + nextIndex + " .cover-center").css("height", coverCentHeight + "px").css("background-size", "100% " + coverCentHeight + " px");
    var covCentImg = $("#section" + nextIndex + " .cover-center img")[0];
    var tmpCentImg = new Image();
    tmpCentImg.src = covCentImg.src;
    var centcentdiv = $("#section" + nextIndex + " .cover-center .cover-centcent");
    if(centcentdiv.height() / centcentdiv.width() > tmpCentImg.height / tmpCentImg.width ){//预留位置比图片高
        $("#section" + nextIndex + " .cover-center .cover-centcent img").css("height", centcentdiv.height());
        $("#section" + nextIndex + " .cover-center .cover-centcent img").css("width", centcentdiv.height() / tmpCentImg.height *  tmpCentImg.width);
        $("#section" + nextIndex + " .cover-center .cover-centcent img").css("margin-left", "-" + ((centcentdiv.height() / tmpCentImg.height *  tmpCentImg.width - centcentdiv.width()) / 2) + "px");
        $("#section" + nextIndex + " .cover-center .cover-centcent img").css("margin-top","0px");
    }
    else{//预留位置比图片扁
        $("#section" + nextIndex + " .cover-center .cover-centcent img").css("width", centcentdiv.width());
        $("#section" + nextIndex + " .cover-center .cover-centcent img").css("height", centcentdiv.width() / tmpCentImg.width *  tmpCentImg.height);
        $("#section" + nextIndex + " .cover-center .cover-centcent img").css("margin-top", "-" + ((centcentdiv.width() / tmpCentImg.width *  tmpCentImg.height - centcentdiv.height()) / 2) + "px");
        $("#section" + nextIndex + " .cover-center .cover-centcent img").css("margin-left","0px");
    }
    var coverBottom = $("#section" + nextIndex + " .cover-bottom").css("height", coverSecHeight * 0.05 + "px");
    //设置行高使得居中显示
    var locHight = $($("#section" + nextIndex + " .cover-location")[0]).height();
    $("#section" + nextIndex + " .cover-location").css("line-height", locHight + "px");
    var dateHight = $($("#section" + nextIndex + " .cover-date")[0]).height();
    $("#section" + nextIndex + " .cover-date").css("line-height", dateHight + "px");
};

window.onload = function (){
    $('#fullpage').fullpage({
        sectionsColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
        'onLeave': function(index, nextIndex, direction){
            //背景
            var visi = nextIndex == 1 ? "hidden" : "visible";
            $("#pic-back").css("visibility", visi);
            $("#pic-back").css("width", $("#section" + nextIndex).width() + "px");
            $("#pic-back").css("left", "50%");
            $("#pic-back").css("margin-left", "-" + ($("#section" + nextIndex).width() / 2) + "px");

            //下一页的排版
            //封面
            if($("#section" + nextIndex + " .cover-sec").length != 0){
                loadFirstPageStructure();
            }
            //目录
            else if($("#section" + nextIndex + " .catalog-sec").length != 0){
                //设置行高使得居中显示
                var headheight = $($("#section" + nextIndex + " .catalog-head")[0]).height();
                $("#section" + nextIndex + " .catalog-head,#section" + nextIndex + " .catalog-content").css("line-height", headheight + "px");
            }
            //图文页
            else if($("#section" + nextIndex + " .pic-des-div").length != 0){
                var currPicHeight;
                var nextPageImg = new Image();
                nextPageImg.src = $("#section" + nextIndex + " .pic-des-pic")[0].src;
                //鉴于已经预加载，此处可以直接用该图像的高宽
                var hw = nextPageImg.height / nextPageImg.width;
                if( hw < 0.8){
                    currPicHeight = 0.48;
                }
                else if(hw >= 0.8 && hw <= 1.3){
                    currPicHeight = 0.6;
                }
                else if(hw > 1.3){
                    currPicHeight = 0.96;
                    $("#section" + nextIndex + " .pic-des-div .pic-des-des div").remove();
                }
                var picdiv = $("#section" + nextIndex + " .pic-des-div .pic-des-picdiv").css("height", currPicHeight * $(document).height() + "px");
                var divImg = $("#section" + nextIndex + " .pic-des-div .pic-des-picdiv img");
                if(picdiv.height() / picdiv.width() > nextPageImg.height / nextPageImg.width ){//预留位置比图片高
                    divImg.css("height", picdiv.height() + "px");
                    divImg.css("width", picdiv.height() / nextPageImg.height *  nextPageImg.width + "px");
                    divImg.css("margin-left", "-" + ((picdiv.height() / nextPageImg.height *  nextPageImg.width - picdiv.width()) / 2) + "px");
                    divImg.css("margin-top","0px");
                }
                else{//预留位置比图片扁
                    divImg.css("width", picdiv.width() + "px");
                    divImg.css("height", picdiv.width() / nextPageImg.width *  nextPageImg.height + "px");
                    divImg.css("margin-top", "-" + ((picdiv.width() / nextPageImg.width *  nextPageImg.height - picdiv.height()) / 2) + "px");
                    divImg.css("margin-left","0px");
                }

                //自动调整下方块数
                var leftHeight = $(document).height() * 0.96 - picdiv.height();//上下共4%的margin
                var picdesdiv = $("#section" + nextIndex + " .pic-des-div .pic-des-des");
                picdesdiv.css("height", leftHeight + "px");
                var ps =  $("#section" + nextIndex + " .pic-des-div .pic-des-des div");
                var maxIndex;
                var divMargin = $("p").height();
                var currDivHeight = 0;
                for(maxIndex = 0; (currDivHeight < leftHeight) && (maxIndex < ps.length); maxIndex++){
                    currDivHeight += $(ps[maxIndex]).height() + divMargin;
                }
                ps.css("margin", divMargin + "px  0px");
                $("#section" + nextIndex + " .pic-des-div .pic-des-des div p").css("margin", "0px")
                $("#section" + nextIndex + " .pic-des-div .pic-des-des div:gt(" + (maxIndex - 1) + ")").remove();
            }
            //尾页
            else if($("#section" + nextIndex + " .end-sec").length != 0){
                //设置行高使得居中显示
                var headheight = $($("#section" + nextIndex + " .end-head")[0]).height();
                $("#section" + nextIndex + " .end-head,#section" + nextIndex + " .end-content").css("line-height", headheight + "px");
                    var moreheight = $($("#section" + nextIndex + " .end-more")[0]).height();
                $("#section" + nextIndex + " .end-more").css("line-height", moreheight + "px");
            }
            //尾页删除箭头
            if($("#section" + nextIndex + " .end-sec").length != 0){
                $("#pageArrow").hide();
            }
            else{
                $("#pageArrow").show();
            }
            //删除上一页的动画并隐藏所有动画要素

            $("#section" + index + " .anim1,#section" + index + " .anim2,#section" + index + " .anim3,#section" + index + " .anim4,#section" + index + " .anim5").removeClass("animated");
            //分组动画时延
            var aniIndex = 1;
            var anistrs = $("#section" + nextIndex + " .marksectype").attr("anitype");
            var aniArray = (!anistrs)? [] : anistrs.split(" ");
            while($("#section" + nextIndex + " .anim" + aniIndex).length != 0){//该section下面有动画
                //设置动画延迟并添加动画完成事件--移除动画类并设置为隐藏
                $("#section" + nextIndex + " .anim" + aniIndex).css("-webkit-animation-delay", (aniIndex * 0.8) + "s");
                $("#section" + nextIndex + " .anim" + aniIndex).each(function (elemIndex, elem){
                    elem.addEventListener("webkitAnimationEnd", function(){
                        $(elem).removeClass("animated");
                        $(elem).removeClass($(elem).attr("curranitype"));
                    });
                });

                $("#section" + nextIndex + " .anim" + aniIndex).addClass(aniArray[aniIndex - 1]);
                $("#section" + nextIndex + " .anim" + aniIndex).attr("curranitype", aniArray[aniIndex - 1]);
                if(!$("#section" + nextIndex + " .anim" + aniIndex).hasClass("animated")){
                    $("#section" + nextIndex + " .anim" + aniIndex).addClass("animated");
                }
                aniIndex++;
            }
        }
    });

    if($(document).width()> $(document).height()){
        $("#fullpage").width($(document).height() * 3 /4 );
        $(".section .pic-des-div .pic-des-des p").css("font-size", "1.5em");
    }
    else{
        $(".section .pic-des-div .pic-des-des p").css("font-size", "2em");
    }
    loadFirstPageStructure();
    $("#pageArrow").css("visibility", "visible");
};
