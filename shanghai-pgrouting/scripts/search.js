showingSearchResults = false;
$(document).ready(function() {
    // 为搜索按钮添加 click 响应
    $('#search-btn').click(function () {
        clear_search_result_layer();
        var searchText = $('#map_search_input').val();
        if (searchText == '') {
            search_alert('请输入关键词！');

        } else {
            search($('#map_search_input').val());
        }
    });

    $('#map_search_input').keyup(function () {
        $(this).parent().removeClass('has-error has-feedback');
        search_prompt($(this).val());

        if ($(this).text() == '') {
            clear_search_result_layer();
        }

    })

    /*
    // google place search service
    var attribution_element = document.getElementById('attribution_div');
    var service_place_api = new google.maps.places.PlacesService(attribution_element);
    var request = {
        location: new google.maps.LatLng(34.2667, 108.9000),
        radius: '50000',
        query: '凉皮',
        language: 'zh-CN',
        types: 'food',
        key: 'AIzaSyDKkztKK3GR9EZgifnW4tPKr0UWu11pbCQ'
    };

    service_place_api.nearbySearch(request, function (results, status) {
        console.log(results + ', ' + status);
    });
    */
});

function clear_search_result_layer () {
    if (typeof placeSearchLayer != 'undefined') {
        placeSearchLayer.getSource().clear();
        // placeSearchLayer.source = null;
    }

    // showVectorLayers();
    showingSearchResults = false;
}


function search(text) {
    /*
    var message = '没有与 "<strong>' + text + '</strong>" 匹配的搜索结果，请修改关键词重新搜索';
    search_alert(message);
    */

    // placessearch.json: /static/silkroad/data/search/places.json
    // searchURL = '/static/silkroad/data/search/places.json';

    // searchURL = 'http://localhost:8000/silkroad/nearbysearch?url=https://maps.googleapis.com/maps/api/place/nearbysearch/json?';

    // ***********************
    // google places api
    // ***********************
    /*
    searchURL = 'http://localhost:8000/silkroad/nearbysearch?';
    var mapCenter = map.getView().getCenter();
    var lonlat = ol.proj.transform(mapCenter, 'EPSG:3857', 'EPSG:4326');
    searchURL += 'location=' + lonlat[1] + ',' + lonlat[0];
    searchURL += '&radius=50000&keyword=' + text + '&language=zh_CN&key=AIzaSyDKkztKK3GR9EZgifnW4tPKr0UWu11pbCQ';

    // searchURL = '/static/silkroad/data/search/西安凉皮.json';

    $('#search-btn').button('loading');
    $('#search-btn').text('正在搜索...');
    $.getJSON(searchURL, null, function ( data ) {
        $('#search-btn').button('reset');
        handle_search_result(data);
    });
    */

    var result_features = [];

    // 遗产点 POI 搜索
    poi_features = poiLayer.getSource().getFeatures();
    poi_features.forEach(function (entry) {
        var name = entry.getProperties()['name'];
        if (name.indexOf(text) > -1) {
            result_features.push(entry);
        }
    });

    food_features = foodLayer.getSource().getFeatures();
    food_features.forEach(function (entry) {
        var name = entry.getProperties()['name'];
        var intro = entry.getProperties()['features'] + entry.getProperties()['intro'];
        if (name.indexOf(text) > -1 || intro.indexOf(text) > -1) {
            result_features.push(entry);
        }

    });

    hotel_features = hotelLayer.getSource().getFeatures();
    hotel_features.forEach(function (entry) {
        var name = entry.getProperties()['name'];

        if (name.indexOf(text) > -1) {
            result_features.push(entry);
        }
    });

    if (result_features.length == 0) {
        search_alert('没有找到匹配的结果，请尝试修改关键词重试。');

        return;
    } else {
        handle_result_features(result_features);
    }


}

function search_prompt(text) {

    if ($('#search_prompt').length > 0) {
        $('#search_prompt').remove();
    }
    if (text == '')
        return;
}




function handle_result_features(result_features) {


    placeSearchOverlay = new ol.FeatureOverlay({
      map: map,
      style: function(feature, resolution) {
        var text = feature.get('name');

        var style = [new ol.style.Style({
            stroke: new ol.style.Stroke({
              // color: '#00f',
              // color: '#DAA520',
              color: '#319FD3',
              width: 1
            }),
            fill: new ol.style.Fill({
              color: 'rgba(255,255,255,0.6)'
            }),
            text: new ol.style.Text({
              font: 'Normal 16px Arial;;',
              text: text,
              fill: new ol.style.Fill({
                color: '#000'
              }),
              stroke: new ol.style.Stroke({
                // color: '#f00',
                // width: 3
                color: '#fff',
                width: 3
              })
            })

        })];
        return style;
      }
    });

    var iconStyle = new ol.style.Style({
      image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
        anchor: [0.5, 46],
        anchorXUnits: 'fraction',
        anchorYUnits: 'pixels',
        opacity: 0.75,
        src: '/static/silkroad/img/map_marker 32x32.png'
      }))
    });

    /*
    $('#btn-group-cities').children('button').click(function () {
        var cityName = $(this).attr('id');
        if (lonlat = cityPositions[cityName]) {
            var coordinate = ol.proj.transform(lonlat, 'EPSG:4326', 'EPSG:3857');
            var pan = ol.animation.pan({
                duration: 500,
                source: (map.getView().getCenter())
            });
            map.beforeRender(pan);
            map.getView().setCenter(coordinate);
        }
    });

    */
    // var json = JSON.parse(data);

    /*
    // 判断是否返回了有效的搜索结果
    if (data.status == 'ZERO_RESULTS')
    {
        search_alert('找不到结果，请修改关键词重试');
        return;
    }
    */



    var vectorSource = new ol.source.Vector({
        features: result_features
    });

    placeSearchLayer = new ol.layer.Vector({
        source: vectorSource,
        style: function(feature, resolution) {
        var text = feature.get('name');

        var style = [new ol.style.Style({
            stroke: new ol.style.Stroke({
              // color: '#00f',
              // color: '#DAA520',
              color: '#319FD3',
              width: 1
            }),
            fill: new ol.style.Fill({
              color: 'rgba(255,255,255,0.6)'
            }),
            text: new ol.style.Text({
              font: 'Normal 16px Arial;',
              text: text,
              fill: new ol.style.Fill({
                color: '#000'
              }),
              stroke: new ol.style.Stroke({
                // color: '#f00',
                // width: 3
                color: '#fff',
                width: 3
              })
            }),
            image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
                anchor: [0.5, 46],
                anchorXUnits: 'fraction',
                anchorYUnits: 'pixels',
                opacity: 1,
                src: '/static/silkroad/img/map_marker 32x32.png'
              }))

        })];
        return style;
      }
    });
    map.addLayer(placeSearchLayer);

    // 在搜索框下方显示的结果列表
    var html = '<div id="search_prompt" class="search_prompt col-md-2"><div class="list-group col-md-12">';
    var i = 0;
    result_features.forEach( function (entry) {
        html += '<a href="#" class="list-group-item">' + '<strong>' + entry.t.name + '</strong>'/* + ': ' + entry.t.vicinity*/ + '</a>';
        i++;

        // 项目太多，显示效果有问题。
        // 目前的数据量不需要做个数限制。以后数据量增加的情况下，可以采用分页的方式。
        /*
        if (i > 6)
        {
            return false;
        }
        else
            return true;
        */
    });

    html += '</div></div>';

    /*
    <div id="search_prompt" class="search_prompt col-md-2">
        <div class="list-group col-md-12">
            <a href="#" class="list-group-item active">Cras justo odio</a>
            <a href="#" class="list-group-item">Dapibus ac facilisis in</a>
            <a href="#" class="list-group-item">Morbi leo risus</a>
            <a href="#" class="list-group-item">Porta ac consectetur ac</a>
            <a href="#" class="list-group-item">Vestibulum at eros</a>
        </div>
    </div>
    */

    /*
    <div id="search_prompt" class="search_prompt col-md-2">
        <div class="list-group col-md-12">
            <a href="#" class="list-group-item active">与sss 有关的第1条结果</a>
            <a href="#" class="list-group-item active">与sss 有关的第2条结果</a>
            <a href="#" class="list-group-item active">与sss 有关的第3条结果</a>
            <a href="#" class="list-group-item active">与sss 有关的第4条结果</a>
            <a href="#" class="list-group-item active">与sss 有关的第5条结果</a>
        </div>
    </div>
    */
    $(html).insertAfter($('#map_header')).fadeIn('slow');

    $('#search_prompt a').each(function (index) {
        var search_result_feature_index = index;
        $(this).click(function () {
            feature = placeSearchLayer.getSource().getFeatures()[search_result_feature_index];
            coordinate = feature.getGeometry()['j'];
            var x = coordinate[0];
            var y = coordinate[1];
            popup_coordinate = [x, y + 500];
            center_coordinate = [x, y + 5000];

            showPopup(popup_coordinate, feature);

            var pan = ol.animation.pan({
            duration: 500,
            source: /** @type {ol.Coordinate} */ (map.getView().getCenter())
                });
            map.beforeRender(pan);
            map.getView().setCenter(coordinate);
        })
    });

    // 定位到第一条结果
    if (map.getView().getZoom() < 10) {
        map.getView().setZoom(10);
    }

    var destination = result_features[0];
    // var lonlat = [destination.getProperties()['lat'], destination.getProperties()['lon']];
    // var lonlat = destination.getProperties()['geometry']
    // var coordinate = ol.proj.transform(lonlat, 'EPSG:4326', 'EPSG:3857');
    var coordinate = destination.getGeometry()['j'];

    var pan = ol.animation.pan({
            duration: 500,
            source: /** @type {ol.Coordinate} */ (map.getView().getCenter())
        });
    map.beforeRender(pan);
    map.getView().setCenter(coordinate);

    hideVectorLayers();
    showingSearchResults = true;
}

function hideVectorLayers() {
    poiLayer.set('visible', false);
    foodLayer.set('visible', false);
    hotelLayer.set('visible', false);
}

function showVectorLayers() {
    poiLayer.set('visible', true);
    foodLayer.set('visible', true);
    hotelLayer.set('visible', true);
}