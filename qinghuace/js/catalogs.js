/**
 * Created by Yan on 2/6/2015.
 */

$(document).ready(function() {
    var width = $('.catalog-decoration').width();
    var height = 0.1 * width;
    $('.catalog-decoration').css('height', height);
    /*
    $('.catalog-decoration').css('margin-top', height / 2);
    $('.catalog-decoration').css('margin-bottom', height / 2);
    */

    var clientWidth = document.documentElement.clientWidth;
    // 根据屏幕宽度确定文字大小
    // 1080: 30px
    $('.catalog-name p').css('font-size', 35 * clientWidth / 1080 + 'px');
    $('.catalog-detail p').css('font-size', 30 * clientWidth / 1080 + 'px');

    $('.catalog').click(function(event) {
        var link = $(this).find('.catalog-anchor').attr('href');
        window.open(link, '_self');
    });

    // 动画
    // 图片完成后开始动画
    $('#catalogs').imagesLoaded(function() {
        // catalog 列表动画
        $('#catalogs').removeClass('am-hide');
        $('.catalog').each(function (index, value) {
            $(this).css("-webkit-animation-delay", index * 0.3 + 's');
            $(this).addClass('am-animation-slide-bottom');
        });

        // 底部导航栏动画
        $('#bottom-nav').removeClass('am-hide');
        $('#nav-item-index').addClass('am-animation-slide-left');
        $('#nav-item-call').addClass('am-animation-slide-right');
    });

});