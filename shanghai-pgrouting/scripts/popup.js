$(document).ready( function () {
	// 为 popup_closer 添加事件响应

    container = document.getElementById('popup');
    content = document.getElementById('popup-content');
    closer = document.getElementById('popup-closer');

    popup_overlay = new ol.Overlay({
            element: container
    });
    map.addOverlay(popup_overlay);


    closer.onclick = function() {
        clearPopup();

        return false;
    };



    /*
    // 为 container 加上 class 与 style 属性
    $('#popup').addClass('ol-popup');
    $('#popup').css({
        'display': 'none'
    });
*/





    map.on('click', function (event) {
    	var coordinate = event.coordinate;
    	// var hdms = ol.coordinate.toStringHDMS(ol.proj.transform(coordinate, 'EPSG: 3857', 'EPSG:4326'));

    	var feature = map.forEachFeatureAtPixel(event.pixel, function (feature, layer) {
    		if (layer){
    			return feature;
    		}

    	});

        if (feature == null) {
            clearPopup();
            return;
        }

        showPopup(coordinate, feature);

    });



});

function clearPopup() {
    content.innerHTML = "";
    container.style.display = 'none';
    $('#popup-closer').blur();
}

function showPopup(coordinate, feature) {

        popup_overlay.setPosition(coordinate);

        var name = feature.get('name');
        if (name == '') {
            name = 'unnamed road';
        }

        // content.innerHTML = '<!-- Nav tabs --><ul class="nav nav-tabs" role="tablist"><li class="active"><a href="#home" role="tab" data-toggle="tab">图文介绍</a></li><li><a href="#profile" role="tab" data-toggle="tab">音视频资料</a></li><li><a href="#messages" role="tab" data-toggle="tab">景区指引</a></li><li><a href="#settings" role="tab" data-toggle="tab">三维展示</a></li></ul><!-- Tab panes --><div class="tab-content"><div class="tab-pane active" id="home">图文介绍</div><div class="tab-pane" id="profile">音视频资料</div><div class="tab-pane" id="messages">景区指引</div><div class="tab-pane" id="settings">三维展示</div></div>';
        content.innerHTML = '<h4><nobr><p>' + name + '</p></nobr></h4>';
        content.innerHTML += '<p><nobr>cost: ' + feature.get('cost').toFixed(3) + ' km</nobr></p>';
        content.innerHTML += '<p><nobr>class: ' + class_ids[feature.get('class_id')] + '</nobr></p>';




    container.style.display = 'block';

    $('#SetRouteSource').click( function (event) {
        event.preventDefault();
        setRouteSource($(this).attr('feature_name'));
    });
    $('#SetRouteDestination').click(function (event) {
        event.preventDefault();
        setRouteDestination($(this).attr('feature_name'));
    });
}

