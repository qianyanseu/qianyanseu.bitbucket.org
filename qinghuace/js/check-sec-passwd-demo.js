/**
 * Created by Yan on 4/10/2015.
 */
$(document).ready(function() {

    $('#button_check_sec').click(function() {

        /*
        vex.dialog.open({
            message: '输入密钥（aaabbb 可通过验证）',
            input: '<input name="password" type="password" placeholder="密钥" required />',
            buttons: [
                $.extend(vex.dialog.buttons.YES, {text: '验证密钥'}),
                $.extend(vex.dialog.buttons.NO, {text: '返回页面'})
            ],
            callback: function(data) {
                if (data) {
                    console.log(data.password);

                    if (data.password == 'aaabbb')
                    {
                        $.extend(vex.dialog.buttons.YES, {text: '跳转到产品目录页……'});
                        vex.dialog.alert('密钥验证成功');
                    }
                    else
                    {
                        $.extend(vex.dialog.buttons.YES, {text: '返回页面'});
                        vex.dialog.alert('密钥不正确');
                    }


                }
                else {
                    console.log('已取消');
                }
            }
        });
        */

        alertify.prompt("请输入解锁码", function (e, str) {
            // str is the input text
            if (e) {
                // user clicked "ok"
                console.log(str);

                // success notification
                // shorthand for alertify.log("Notification", "success");
                if (str == 'aaabbb') {
                    alertify.success("解锁码验证成功，后续跳转到产品目录页……");
                }
                else {
                    alertify.error("解锁码验证失败……");
                }


                // error notification
                // shorthand for alertify.log("Notification", "error");

            } else {
                // user clicked "cancel"
            }
        }, "");
    });
});