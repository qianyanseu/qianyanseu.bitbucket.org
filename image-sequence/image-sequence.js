/**
 * Created by Yan on 4/20/2015.
 */
function rightNow() {
  if (window['performance'] && window['performance']['now']) {
    return window['performance']['now']();
  } else {
    return +(new Date());
  }
}

var fps          = 30,
    currentFrame = 0,
    totalFrames  = 75,
    img          = document.getElementById("myImage"),
    currentTime  = rightNow();

(function animloop(time){
  var delta = (time - currentTime) / 1000;

  currentFrame += (delta * fps);

  var frameNum = Math.floor(currentFrame);

  if (frameNum >= totalFrames) {
    currentFrame = frameNum = 0;
  }

    if (currentFrame < totalFrames) {
        requestAnimationFrame(animloop);
    }


  img.src = "images/Comp%202_000" + (frameNum < 10 ? "0" : "") + frameNum + ".jpg";
    // console.log("images/Comp%202_000" + (frameNum < 10 ? "0" : "") + frameNum + ".jpg");

  currentTime = time;


})(currentTime);