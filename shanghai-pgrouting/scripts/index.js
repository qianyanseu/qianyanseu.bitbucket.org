$(document).ready( function () {
    console.log('map.js ready!');

    map = new ol.Map({
        target: 'map-element',
        layers: [
    		new ol.layer.Tile({
    			source : new ol.source.TileJSON({
    				url: 'https://api.tiles.mapbox.com/v3/qianyanseu.jomlod6d.jsonp?access_token=pk.eyJ1IjoicWlhbnlhbnNldSIsImEiOiJsNnE1YVhrIn0.Tf_DvFH4ZP26QflrFxSBmA',
    				crossOrigin: 'anonymous'
    			})
    		})
        ],
        view: new ol.View({
            center: [13528852.70518923, 3663618.14253868],
            zoom: 10
        })
    });

    // var bbox source =
    bboxLayer = new ol.layer.Vector({
        source: new ol.source.GeoJSON({
            projection: 'EPSG:3857',
            url: 'resources/data/bbox_line.geojson'
        }),
        style: new ol.style.Style({
            fill: new ol.style.Fill({
                color: 'rgba(255, 255, 255, 0.3)'
            }),
            stroke: new ol.style.Stroke({
                color: '#d95f0e',
                width: 2
            })
        })
    });
    map.addLayer(bboxLayer);

    transform = ol.proj.getTransform('EPSG:3857', 'EPSG:4326');



    startPoint = new ol.Feature();
    startPoint.setId('start');
    finalPoint = new ol.Feature();
    finalPoint.setId('destination');

    pointOverlay = new ol.FeatureOverlay({
        features: [startPoint, finalPoint],

        style: function(feature) {
            var id = feature.getId();
            var image_src = 'resources/icon/';
            if (id == 'start') {
                image_src += 'source.png';
            } else if (id == 'destination') {
                image_src += 'destination.png';
            }

            var style = new ol.style.Style({
                image: new ol.style.Icon({
                    anchor: [0.5, 1],
                    anchorXUints: 'fraction',
                    anchorYUnits: 'fraction',
                    opacity: 1,
                    src: image_src
                })
            });

            return [style];
        },

        map: map});

    highlight = null;
    highlightOverlay = new ol.FeatureOverlay({
        map: map,
        style: new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: 'rgba(0, 255, 0, 0.6)',
                width: 6
            })
        })
    });

    $(map.getViewport()).on('mousemove', function(event){

        highlightFeatureAtPixel(event);

        var pixel = map.getEventPixel(event.originalEvent);
        var hit = map.forEachFeatureAtPixel(pixel, function(feature, layer) {
            if (!(typeof result === 'undefined') && layer == result ) {
                return true;
            }

        });
        if (map_click_mode == 'normal') {
            if (hit) {
            document.getElementById(map.getTarget()).style.cursor = 'pointer';
        } else {
            document.getElementById(map.getTarget()).style.cursor = '';
        }
        }

    })
    var highlightFeatureAtPixel = function(event) {
        var pixel = map.getEventPixel(event.originalEvent);
        var feature = map.forEachFeatureAtPixel(pixel, function(feature, layer) {
            return feature;
        });

        if (feature !== highlight) {
            if (highlight) {
                highlightOverlay.removeFeature(highlight);
                // clearPopup();
            }
            if (feature) {
                highlightOverlay.addFeature(feature);
                // showPopup(event.coordinate, feature);
            }
            highlight = feature;
        }
    }




    // map click handler
    // click to set source or destination
    reset_map_click_mode();
    map.on('click', function(event) {
        if (/*startPoint.getGeometry() == null*/map_click_mode == 'start') {
            // first click
            startPoint.setGeometry(null);
            startPoint.setGeometry(new ol.geom.Point(event.coordinate));


            var startCood = transform(startPoint.getGeometry().getCoordinates());
            map_alert("Startpoint set at: " + startCood[0].toFixed(2) + ', ' + startCood[1].toFixed(2), 'success');
        }

        else if (/*finalPoint.getGeometry() == null*/map_click_mode == 'destination') {
            // second click
            finalPoint.setGeometry(null);
            finalPoint.setGeometry(new ol.geom.Point(event.coordinate));


            var startCood = transform(startPoint.getGeometry().getCoordinates());
            map_alert("Destination set at: " + startCood[0].toFixed(2) + ', ' + startCood[1].toFixed(2), 'success');
        }

        if (map_click_mode != 'normal') {
            search_route(startPoint, finalPoint);
        };

        reset_map_click_mode();
    });


    // button click handler
    $('#btn-start').click(function() {
        set_map_click_mode('start');
        map_alert('Click the map to select source point...', 'info');
        $(this).blur();
    });

    $('#btn-destination').click(function() {
        set_map_click_mode('destination');
        map_alert('Click the map to select destination point...', 'info');
        $(this).blur();
    });

    $('#btn-clear').click(function() {
        console.log('cleared');
        // hide the overlays
        startPoint.setGeometry(null);
        finalPoint.setGeometry(null);

        // remove the result layer
        if (typeof result != 'undefined') {
            map.removeLayer(result);
        }


        clearPopup();
        clearRouteDetail();

        map_alert('Result cleared!', 'success');
        $(this).blur();
    });
});

function search_route(startPoint, finalPoint) {

    var params = {
        LAYERS: 'pgrouting:fromAtoB',
        FORMAT: 'image/png'
    }


    if (startPoint.getGeometry() == null || finalPoint.getGeometry() == null) {
        return;
    }

    var minX, maxX, minY, maxY;
    minX = startPoint.getGeometry().getCoordinates()[0];
    minY = startPoint.getGeometry().getCoordinates()[1];
    maxX = finalPoint.getGeometry().getCoordinates()[0];
    maxY = finalPoint.getGeometry().getCoordinates()[1];

    if (minX > maxX) {
        var temp = minX;
        minX = maxX;
        maxX = temp;
    }
    if (minY > maxY) {
        var temp = minY;
        minY = maxY;
        maxY = temp;
    }

    /*
    map.getView().fitExtent([minX, minY, maxX, maxY], map.getSize());
    map.getView().setZoom(map.getView().getZoom() - 1);
    */



    map_alert('Startpoint and destination both set, beginning route searching...', 'info')
    var startCoord = transform(startPoint.getGeometry().getCoordinates());
    var finalCoord = transform(finalPoint.getGeometry().getCoordinates());
    var viewparams = [
        'x1:' + startCoord[0], 'y1:' + startCoord[1],
        'x2:' + finalCoord[0], 'y2:' + finalCoord[1]
    ];
    params.viewparams = viewparams.join(';');

    // we now have the two points, create the result layer and add it to the map
    /*
    result = new ol.layer.Image({
        source: new ol.source.ImageWMS({
            url: 'http://thegisguy.info:8080/geoserver/pgrouting/wms',
            params: params
        })
    });
    map.addLayer(result);
    */

    vectorSource = new ol.source.ServerVector({
        format: new ol.format.GeoJSON(),
        loader: function(extent, resolution, projection) {
            var url = 'http://www.thegisguy.tk:8080/geoserver/wfs?service=WFS&' +
        'version=1.1.0&request=GetFeature&typename=pgrouting:fromAtoB&' +
        'outputFormat=text/javascript&format_options=callback:loadFeatures' +
        '&srsname=EPSG:3857&bbox=' + extent.join(',') + ',EPSG:3857&viewparams=' + viewparams.join(';');

            /*var url = 'http://www.thegisguy.tk:8080/geoserver/wfs?service=WFS&' +
        'version=1.1.0&request=GetFeature&typename=pgrouting:fromAtoB&' +
        'outputFormat=text/javascript&format_options=callback:loadFeatures' +
        '&srsname=EPSG:3857&viewparams=' + viewparams.join(';');*/
            $.ajax({
                url: url,
                dataType: 'jsonp',
                timeout: 10000,
                success: function() {
                    console.log('ajax success');

                },
                error: function(xhr, status, error) {
                    // 无论是否加载成功，status 都是 error，因此总是调用 error 回调函数
                    if (status == 'timeout') {
                        map_alert('Server timeout, check your connection...', 'warning');
                        return;
                    }

                    map_alert('Route search successful, results loaded...', 'success');

                    showRouteDetail();
                }
            });
        },

        stragety: ol.loadingstrategy.createTile(new ol.tilegrid.XYZ({
            maxZoom: 19
        })),

        //stragety: ol.loadingstrategy.all(),
        projection: 'EPSG: 3857'
    });

    console.log(vectorSource.getState());

    vectorSource.on('change', function(event) {
        console.log(vectorSource.getState());
    });

    loadFeatures = function(response) {
        vectorSource.addFeatures(vectorSource.readFeatures(response));
    }

    if (!(typeof result === 'undefined')) {
        map.removeLayer(result);
    }

    result = new ol.layer.Vector({
        source: vectorSource,
        style: new ol.style.Style({
            fill: new ol.style.Fill({
                color: 'rgba(0, 255, 0, 0.8)'
            }),
            stroke: new ol.style.Stroke({
                color: 'rgba(0, 0, 255, 0.5)',
                width: 8
            })
        })
    });

    map.addLayer(result);
    // map.getView().fitExtent(result.getSource().getExtent(), map.getSize());

    // map.getView().fitExtent(vectorSource.getExtent(), map.getSize());
    map.getView().fitExtent([minX, minY, maxX, maxY], map.getSize());
    map.getView().setZoom(map.getView().getZoom() - 1);



}

function reset_map_click_mode() {
    map_click_mode = 'normal';
}

function set_map_click_mode(mode) {
    if(mode == 'start' || mode == 'destination') {
        map_click_mode = mode;
        document.getElementById(map.getTarget()).style.cursor = 'crosshair';
    }
}

function clearRouteDetail() {
    $('#route_detail').hide("slide", { direction: "right" }, 300, function() {
        restoreMap();
    });

    // $('#route_detail').hide();
    // $(this).hide("slide", { direction: "left" }, 1000);
    // $('#route_detail_meta').text('Currently no route.');
    $('#route_detail_heading').text('Route Detail');
    $('#route_detail_tbody').empty();
}

function showRouteDetail() {
    shrinkMap();
    $('#route_detail').show("slide", { direction: "right" }, 300);

    $('#route_detail_tbody').empty();

    var features = result.getSource().getFeatures();
    /*
    var seqs = new Array();

    features.forEach(function (feature, index) {
        // seqs.push(feature.get('seq'));
        var seq = feature.get('seq');
        // seqs[seq.toString()] = index;
        seqs.push({seq: index});
    });

    seqs.sort(function (a, b) {
        return a[0] - b[0];
    });
    */



    features.sort(function (a, b) {
        return a.get('seq') - b.get('seq');
    });

    // var features_unique = [];
    var seqs = {};
    features.forEach(function(feature, index) {
        seqs[feature.get('seq')] = index;
    });

    var seqsKeys = Object.keys(seqs);
    var numOfSegments = seqsKeys.length;
    var totalCost = 0;
    // features.forEach(function (feature) {
    seqsKeys.forEach(function (key) {
        // feature = features[seqs[seq]];
        feature = features[seqs[key]];
        var trElement = '<tr>';
        trElement += '<td class="seq">' + feature.get('seq') + '</td>';
        trElement += '<td>' + feature.get('gid') + '</td>';
        trElement += '<td>' + feature.get('name') + '</td>';
        trElement += '<td>' + feature.get('heading').toFixed(0) + '</td>';
        trElement += '<td>' + feature.get('cost').toFixed(3) + '</td>';
        trElement += '<td>' + class_ids[feature.get('class_id')] + '</td>';
        trElement += '</tr>';

        totalCost += feature.get('cost');

        $('#route_detail_tbody').append(trElement);
    });

    // $('#route_detail_meta').html('<p>The route contains <strong>' + features.length + '</strong> segments, with a total cost of <strong>' + totalCost.toFixed(2) + '<strong> km.</p>');
    $('#route_detail_heading').text('Route Detail - ' + numOfSegments + ' segments, ' + totalCost.toFixed(2) + ' km');

    $("tr").hover(
      function () {
        $(this).css("background","yellow");
        var seq = $(this).find('.seq').text();
        var feature = null;
        for (i = 0; i < features.length; i++)
        {
            feature = features[i];
            if (feature.get('seq') == seq)
                break;
        }

        if (feature !== highlight) {
            if (highlight) {
                highlightOverlay.removeFeature(highlight);
                // clearPopup();
            }
            if (feature) {
                highlightOverlay.addFeature(feature);
                // showPopup(event.coordinate, feature);
            }
            highlight = feature;
        }
    },
    function () {
        $(this).css("background","");

        var seq = $(this).find('.seq').text();
        var feature = null;
        for (i = 0; i < features.length; i++)
        {
            feature = features[i];
            if (feature.get('seq') == seq)
                break;
        }
        highlightOverlay.removeFeature(feature);
    });

    $("tr").click(
      function () {
        var seq = $(this).find('.seq').text();
        var feature = null;
        for (i = 0; i < features.length; i++)
        {
            feature = features[i];
            if (feature.get('seq') == seq)
                break;
        }

        clearPopup();
        var numOfCoordinates = feature.getGeometry().getCoordinates().length;
        var coordinate = feature.getGeometry().getCoordinates()[Math.floor(numOfCoordinates / 2)];
        showPopup(coordinate, feature);

        var pan = ol.animation.pan({
                duration: 500,
                source: /** @type {ol.Coordinate} */ (map.getView().getCenter())
            });
            map.beforeRender(pan);
            map.getView().setCenter(feature.getGeometry().getFirstCoordinate());
    });

     $("tr").dblclick(
      function () {
        var seq = $(this).find('.seq').text();
        var feature = null;
        for (i = 0; i < features.length; i++)
        {
            feature = features[i];
            if (feature.get('seq') == seq)
                break;
        }

        map.getView().fitExtent(feature.getGeometry().getExtent(), map.getSize());
    });



}

function shrinkMap() {
    $('#map-element').removeClass('col-md-12');
    $('#map-element').addClass('col-md-8');

    map.updateSize();


}

function restoreMap() {
    $('#map-element').removeClass('col-md-8');
    $('#map-element').addClass('col-md-12');

    map.updateSize();
}


