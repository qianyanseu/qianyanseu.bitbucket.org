
// 尺寸

$(document).ready(function() {

    /*
    // 用 slick 做图片轮播
    $('#carousel').slick({
        dots: true,

        autoplay: false,
        arrows: false,
        centerMode: true,
        lazyLoad: 'ondemand',
        adaptiveHeight: false
    });
    // $('#carousel').imageLoaded(function() {

    // });


    // 屏幕高度
    var clientHeight = document.documentElement.clientHeight;
    var clientWidth = document.documentElement.clientWidth;

    // 屏幕分辨率
    var screenHeight = screen.height;
    var screenWidth = screen.width;
    var heightRatio = 0.8;

    // 像素换算比
    var devicePixelRatio = window.devicePixelRatio;
    // $('.am-viewport').css('height', heightRatio * height);
    // $('#carousel').css('height', heightRatio * screenHeight + 'px');

    $('.slick-track').css('height', heightRatio * screenHeight * devicePixelRatio);



    // $('#product-info').css('height', 0.3 * screenHeight + 'px');

    $('#product-info').css('bottom', $('slick-track').height() + 'px');
    // $('#product-info').css('width', screenWidth);
    */

    // 用 amazeUI 做图片轮播


});


$(document).ready(function() {
    /*
    $('#carousel').slick({
        dots: true,

        autoplay: true,
        arrows: false,
        centerMode: true,
        //lazyLoad: 'ondemand'
        adaptiveHeight: true
    });
    // $('#carousel').imageLoaded(function() {
        var width = $('.slick-slide img').width();
        $('#product-info').css('width', width);
    // });
    */

    // 屏幕高度
    var clientHeight = document.documentElement.clientHeight;
    var clientWidth = document.documentElement.clientWidth;

    // 屏幕分辨率
    var screenHeight = screen.height;
    var screenWidth = screen.width;
    var heightRatio = 1;

    // 像素换算比
    // var devicePixelRatio = window.devicePixelRatio;
    // $('.am-viewport').css('height', heightRatio * height);
    // $('#carousel').css('height', heightRatio * screenHeight * devicePixelRatio + 'px');

    $('#carousel').css('height', heightRatio * clientHeight + 'px');


    //$(this).css("-webkit-animation-delay", index * 0.05 + 's');
    $('.am-slider').flexslider({
        slideShow: false,
        controlNav: false,
        directionNav: false,

        start: function() {
            $('.am-viewport').css('height', heightRatio * clientHeight + 'px');
            $('.slide-image-container').css('height', heightRatio * clientHeight + 'px');
            $('.slide-image-container img').css('vertial-align', 'middle');

            $('#product-info').removeClass('am-hide');
            $('#product-info').addClass('am-animation-fade');

            // 图片过高时允许上下滑动
            $('.am-viewport').css('overflow-y', 'scroll');
        }

        /*
        // 左右滑动之后重置滚动高度。下面这两个方法都没有实现。
        before: function() {
            window.scrollTo(0, 0);
        },
        after: function() {
            window.scrollTo(0, 0);
            $('.am-viewport').scrollTo(0, 0);
        }
        */

    });

    $('.am-slider').css('background', 'transparent');
    $('.am-slider').css('min-height', '80%');
    $('.am-slider').css('height', '80%');

    $('#product-info').css("-webkit-animation-delay", 0.5 + 's');


    // 处理动画
    $('#product-info').on($.AMUI.support.animation.end, function() {

        var animationDelay = 0.3;



        if ($('#product-info').hasClass('am-animation-fade')) {
            $('#product-name').removeClass('am-hide').removeClass('opacity-zero').css("-webkit-animation-delay", animationDelay + 's').addClass('am-animation-scale-up');
            $('#product-description').removeClass('am-hide').removeClass('opacity-zero').css("-webkit-animation-delay", animationDelay + 's').addClass('am-animation-scale-up');

            $('#nav-item-index').removeClass('am-hide').removeClass('opacity-zero').css("-webkit-animation-delay", animationDelay + 's').addClass('am-animation-scale-up');
            $('#nav-item-call').removeClass('am-hide').removeClass('opacity-zero').css("-webkit-animation-delay", animationDelay + 's').addClass('am-animation-scale-up');
        }


        $('#product-info').removeClass('am-animation-fade');

    });


    $('#product-name').on($.AMUI.support.animation.end, function() {
        $('#product-name').removeClass('am-animation-slide-left');
    });

    $('#product-description').on($.AMUI.support.animation.end, function() {
        $('#product-description').removeClass('am-animation-slide-right');
    });

    $('#nav-item-index').on($.AMUI.support.animation.end, function() {
        $('#nav-item-index').removeClass('am-animation-slide-left');
    });

    $('#nav-item-call').on($.AMUI.support.animation.end, function() {
        $('#nav-item-call').removeClass('am-animation-slide-right');
    });







    $('#carousel').imagesLoaded(function() {
    });



    // 点击图片显示/隐藏文字说明
    $('#carousel').click(function() {
        /*
        $('#product-name').slideToggle();
        $('#product-description').slideToggle();
        */
        $('#product-info').fadeToggle('fast', function() {

        });

    });

    // 根据宽度确定字体大小
    $('#product-name').css('font-size', 50 * 1080 / clientWidth + 'px');
    $('#product-description').css('font-size', + 30 * 1080 / clientWidth + 'px');

    // 为 #product-info 设置最大高度
    // $('#product-info').css('max-height', 0.3 * clientHeight + 'px');



    $('#product-description').css('max-height', 0.2 * clientHeight + 'px');
    $('#product-description').css('overflow', 'scroll');

    /*
    $('#nav-btn-index').click(function(event) {
        event.preventDefault();

        // 退场动画
        // $('#product-info').addClass('am-animation-slide-bottom am-animation-reverse');
        $('#nav-item-index').removeClass('am-animation-slide-left').addClass('am-animation-slide-left am-animation-reverse');
        $('#nav-item-call').removeClass('am-animation-slide-right').addClass('am-animation-slide-right am-animation-reverse');
    });
    */
});
