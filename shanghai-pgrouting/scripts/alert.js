function map_alert(message, type) {
    console.log('alert.js ready!');

    var topOffset = 60;

    $('.map_alert').each(function (index) {
        // topOffset += $(this).height() + 20;
        $(this).remove();

    });
    // 下面的  $alert 必须加上 var 声明，否则会被看成全局变量，调用本函数会覆盖前面已经存在的 $alert 变量
    // 参考：http://coolshell.cn/articles/7480.html
    if (type == "warning") {
    var $alert = $('<div class="alert alert-danger alert-dismissable map_alert"><!--<button type="button" class="close" data-dismiss="alert" aria-hidden="false">×</button>-->' + message + '</div>')
        .prependTo($('#map_div_container'));
    } else if (type == "success") {
        var $alert = $('<div class="alert alert-success alert-dismissable map_alert"><!--<button type="button" class="close" data-dismiss="alert" aria-hidden="false">×</button>-->' + message + '</div>')
        .prependTo($('#map_div_container'));
    } else if (type == "info") {
        var $alert = $('<div class="alert alert-info alert-dismissable map_alert"><!--<button type="button" class="close" data-dismiss="alert" aria-hidden="false">×</button>-->' + message + '</div>')
        .prependTo($('#map_div_container'));
    }

    // var origionalTop = $alert.position().top;

    $alert.css({
        //'margin-left': -$(this).width() / 2
        'top': /*origionalTop + */ topOffset + 'px'

    });
    $alert.hide().fadeIn('slow');

    // disappear in 3 seconds
    setTimeout(function () {
        $alert.fadeOut('slow', function () {
            $alert.remove();
        });
    }, 3000);

}
