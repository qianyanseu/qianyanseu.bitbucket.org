$(document).ready(function () {
    // var videoDuration = 85;

    // 打开页面 等待视频长度+6s 判断是否已经使用 videojs 播放页面中的视频。
    // setTimeout("timeOut()", (videoDuration + 6) * 1000);

    // video.js 事件响应

    var player = videojs('SUV');
    // $('.vjs-progress-control').hide();

    // 检测播放时间
    player.on('timeupdate', videoPlayTimeUpdate);
    player.on('ended', videoPlayEnded);

    // 完成事件
    var videoElement = document.getElementById('SUV');
    // videoElement.onended = videoPlayEnded();
    videoElement.addEventListener("timeupdate", function() {
        console.log('原生 timeupdate 事件');
    }, false);
    videoElement.addEventListener("ended", function() {
        console.log('原生 ended 事件');
    }, false);

    // 开放播放时隐藏按钮
    player.on('play', function() {
        // $('#debug').text('开始播放')
        $('#button-forward').removeClass("hidden");
        $('#button-forward').addClass("hidden");
    });

    // 暂停时显示播放按钮
    // 没有起作用
    /*
    player.on('pause', function() {
        player.bigPlayButton.hide();
        player.bigPlayButton.show();
    });
    */

    // 按钮事件响应
    $('#button-replay').click(function (event) {
        event.preventDefault();

        // 重置 video player
    });
    $('#button-forward').click(function (event) {
        event.preventDefault();

        // 打开新页面
        window.location.href="http://www.thehub.com.cn/weiloushu/";
    });
});

function videoPlayEnded() {
    // 播放结束
    cancelFullScreen();
    // $('#buttons').fadeIn();
    // $('#button-forward').show();
    $('#button-forward').removeClass('hidden');
    $('#button-forward').addClass('animated pulse');
    $('#button-forward').text('访问页面');

    $('#SUV').hide();
};

function videoPlayTimeUpdate() {
    var player = videojs('SUV');
    $('#SUV').attr('videojs', 'true');
    // $('#debug').text(player.currentTime() + ' / ' + player.duration());

    // $('#button-forward').removeClass('hidden');
    $('#button-forward').text(player.currentTime() + ' / ' + player.duration());
    if (player.duration() != 0 && player.currentTime() === player.duration()) {
        videoPlayEnded();
    }
}

/*
function timeOut() {

    // 如果没有使用 videojs 播放，超时后也认为视频已经播放完成。
    if ($('#SUV').attr('videojs') == "false") {
        videoPlayEnded();
        console.log('超时');
        $('#debug').append($('#buttons'));
        $('#debug').css('height', window.innerHeight + 'px');
    }

    // $('#debug').text($('html').html());
}
*/

function cancelFullScreen() {
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
    } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
    }
}
