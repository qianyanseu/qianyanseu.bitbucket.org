# qianyanseu.bitbucket.org

My public repository hosted on bitbucket.org.

## OSM/pgRouting Playground

A simple routing app built on top of pgRouting using OSM data.

Visit the site at <http://qianyanseu.bitbucket.org/shanghai-pgrouting/index.html>.

