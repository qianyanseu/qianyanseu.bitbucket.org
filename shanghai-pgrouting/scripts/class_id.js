class_ids = {
	100: "road",
	101: "motorway",
	102: "motorway_link",
	103: "motorway_junction",
	104: "trunk",
	105: "trunk_link",
	106: "primary",
	107: "primary_link",
	108: "secondary",
	109: "tertiary",
	110: "residential",
	111: "living_street",
	112: "service",
	113: "track",
	114: "pedestrian",
	115: "services",
	116: "bus_guideway",
	117: "path",
	118: "cycleway",
	119: "footway",
	120: "bridleway",
	121: "byway",
	122: "steps",
	123: "unclassified",
	124: "secondary_link",
	125: "tertiary_link",
	201: "lane",
	202: "track",
	203: "opposite_lane",
	204: "opposite",
	301: "grade1",
	302: "grade2",
	303: "grade3",
	304: "grade4",
	305: "grade5",
	401: "roundabout"
};
