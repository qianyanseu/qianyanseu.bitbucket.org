/**
 * Created by Yan on 5/7/2015.
 */

var loadedCount = 0;

var imgLoaded = function(){
    loadedCount ++;
    var imgAll = $("body img");
    var proBarLength = $("#loadProgBar").width();
    $("#leftProgBar").css("width", (loadedCount / imgAll.length * proBarLength) + "px");
    if(loadedCount == imgAll.length){
        //全部图片加载完成
        $("#fullpage").fadeIn();
        $("#loadProgBar").fadeOut();
        $("body").addClass("fadeInUp");
    }
};
$(document).ready(
    function(){
        var imgAll = $("body img");
        var imgCount = imgAll.length;
        for(var i = 0; i < imgCount; i++){
            var curImg = new Image();
            curImg.onload = imgLoaded;
            curImg.src = imgAll[i].src;
        }
    }
);

var loadFirstPageStructure = function(){
    var nextIndex =1 ;
    var coverTopWidth = $("#section" + nextIndex + " .cover-top").width();
    var covTopImg = $("#section" + nextIndex + " .cover-top img")[0];
    var tmpImg = new Image();
    tmpImg.src = covTopImg.src;
    //按头的比例设置上部分高度
    var coverTop = $("#section" + nextIndex + " .cover-top").css("height", tmpImg.height * coverTopWidth / tmpImg.width + "px");
    $("#section" + nextIndex + " .cover-top img").css("width","100%").css("height", "100%");
    var coverSecHeight =  $("#section" + nextIndex).height();
    var coverCentHeight = $(document).height() * 0.97 - coverTop.height();
    var coverCent = $("#section" + nextIndex + " .cover-center").css("height", coverCentHeight + "px").css("background-size", "100% " + coverCentHeight + " px");
    var covCentImg = $("#section" + nextIndex + " .cover-center img")[0];
    var tmpCentImg = new Image();
    tmpCentImg.src = covCentImg.src;
    var centcentdiv = $("#section" + nextIndex + " .cover-center .cover-centcent");
    if(centcentdiv.height() / centcentdiv.width() > tmpCentImg.height / tmpCentImg.width ){//预留位置比图片高
        $("#section" + nextIndex + " .cover-center .cover-centcent img").css("height", centcentdiv.height());
        $("#section" + nextIndex + " .cover-center .cover-centcent img").css("width", centcentdiv.height() / tmpCentImg.height *  tmpCentImg.width);
        $("#section" + nextIndex + " .cover-center .cover-centcent img").css("margin-left", "-" + ((centcentdiv.height() / tmpCentImg.height *  tmpCentImg.width - centcentdiv.width()) / 2) + "px");
        $("#section" + nextIndex + " .cover-center .cover-centcent img").css("margin-top","0px");
    }
    else{//预留位置比图片扁
        $("#section" + nextIndex + " .cover-center .cover-centcent img").css("width", centcentdiv.width());
        $("#section" + nextIndex + " .cover-center .cover-centcent img").css("height", centcentdiv.width() / tmpCentImg.width *  tmpCentImg.height);
        $("#section" + nextIndex + " .cover-center .cover-centcent img").css("margin-top", "-" + ((centcentdiv.width() / tmpCentImg.width *  tmpCentImg.height - centcentdiv.height()) / 2) + "px");
        $("#section" + nextIndex + " .cover-center .cover-centcent img").css("margin-left","0px");
    }
    var coverBottom = $("#section" + nextIndex + " .cover-bottom").css("height", coverSecHeight * 0.05 + "px");
    //设置行高使得居中显示
    var locHight = $($("#section" + nextIndex + " .cover-location")[0]).height();
    $("#section" + nextIndex + " .cover-location").css("line-height", locHight + "px");
    var dateHight = $($("#section" + nextIndex + " .cover-date")[0]).height();
    $("#section" + nextIndex + " .cover-date").css("line-height", dateHight + "px");
};

window.onload = function (){
			$('#fullpage').fullpage({
				sectionsColor: ['#f2f2f2', '#4BBFC3', '#7BAABE', 'whitesmoke', '#ccddff'],
                'afterLoad': function(achorLink, index){
                //    //下一页的图片预加载
                //    var curImgCnt = $("#section" + index + " img").length;
                //    for(var curImgIndex = 0; curImgIndex < curImgCnt; curImgIndex++){
                //        var curImg = new Image();
                //        if(curImgIndex == curImgCnt - 1){
                //            curImg.onload = function(elem){
                //                //加载下一页的图片
                //            };
                //        }
                //        curImg.src = $("#section" + index + " img")[curImgIndex].src;
                //    }
                    //手机上翻页动画有问题
                    //$("#section" + (index + 1)).removeClass("animated");
                    //$("#section" + (index - 1)).removeClass("animated");
                },
				'onLeave': function(index, nextIndex, direction){
                    //if(!$("#section" + index).hasClass("zoomOutUp")){
                    //    $("#section" + index).addClass("zoomOutUp");
                    //}
                    //$("#section" + index).addClass("animated");
                    //下一页的排版
                    var nextImgWidth;
                    //封面
                    if($("#section" + nextIndex + " .cover-sec").length != 0){
                        loadFirstPageStructure();
                    }
                    //目录
                    else if($("#section" + nextIndex + " .catalog-sec").length != 0){
                        //设置行高使得居中显示
                        var headheight = $($("#section" + nextIndex + " .catalog-head")[0]).height();
                        $("#section" + nextIndex + " .catalog-head,#section" + nextIndex + " .catalog-content").css("line-height", headheight + "px");
                    }
                    //图文页
                    else if($("#section" + nextIndex + " .pic-des-div").length != 0){
                        var currPicHeightType;
                       if($("#section" + nextIndex + " .pic-des-sec").length != 0){
                            //还未适配指定的图文类时自动适配为特定的图文类
                            var nextPageImg = new Image();
                            nextPageImg.src = $("#section" + nextIndex + " .pic-des-pic")[0].src;
                            //鉴于已经预加载，此处可以直接用该图像的高宽
                            var hw = nextPageImg.height / nextPageImg.width;
                            if( hw < 0.8){
                                currPicHeightType = "lp";
                            }
                            else if(hw >= 0.8 && hw <= 1.3){
                                currPicHeightType = "mp";
                            }
                            else if(hw > 1.3){
                                currPicHeightType = "hp";
                            }
                           $("#section" + nextIndex + " .pic-des-sec").removeClass("pic-des-sec").addClass("pic-des-" + currPicHeightType + "-sec");
                           $("#section" + nextIndex + " .pic-des-pic").removeClass("pic-des-pic").addClass("pic-des-" + currPicHeightType + "-pic");
                           $("#section" + nextIndex + " .pic-des-des").removeClass("pic-des-des").addClass("pic-des-" + currPicHeightType + "-des");
                       }
                       else if($("#section" + nextIndex + " .pic-des-lp-pic").length != 0){
                            currPicHeightType = "lp";
                        }
                        else if($("#section" + nextIndex + " .pic-des-mp-pic").length != 0){
                            currPicHeightType = "mp";
                        }
                        else if($("#section" + nextIndex + " .pic-des-hp-pic").length != 0){
                            currPicHeightType = "hp";
                        }
                       switch(currPicHeightType){//针对不同高度的图只保留相应行数的文字描述
                           case "lp":
                               $("#section" + nextIndex + " .pic-des-mp-des div:gt(2)").each(function(index ,val){
                                $(val).remove();
                               });
                    	   break;
                           case "mp":
                               $("#section" + nextIndex + " .pic-des-mp-des div:gt(1)").each(function(index ,val){
                                $(val).remove();
                               });
                               break;
                           case "hp":
                               $("#section" + nextIndex + " .pic-des-hp-des div").each(function(index ,val){
                                $(val).remove();
                               });
                               break;
                    	   default:
                    		   break;
                       }

                        nextImgWidth = $("#section" + nextIndex + " .pic-des-" + currPicHeightType + "-pic")[0].width;
                        var nextImgHeight = $("#section" + nextIndex + " .pic-des-" + currPicHeightType + "-pic")[0].height;
                        $("#section" + nextIndex + " .pic-des-" + currPicHeightType + "-des").css("width",nextImgWidth);
                        //图文页的图片上下有2%的margin
                        var leftBtmHeight = $(document).height() * 0.96 - nextImgHeight;
                        ////单图时设置下排的文字，设中间为一隔，则最上空半隔，最下空半隔加一行字高（文字头）
                        //var h4Height = $("#section" + nextIndex + " .pic-des-des p").height();
                        //leftBtmHeight = leftBtmHeight - h4Height - h4Height;
                        //var nextsech4s = $("#section" + nextIndex + " .pic-des-" + currPicHeightType + "-des p");
                        //$("#section" + nextIndex + " .pic-des-" + currPicHeightType + "-des").css("height", leftBtmHeight);
                        //var perH4Height = leftBtmHeight / (nextsech4s.length + 1);
                        //for(var l = 0; l < nextsech4s.length; l++){
                        //    $(nextsech4s[l]).css("top",  (perH4Height * 0.5 + (l * perH4Height)) + "px");
                        //}
                        //上下各留半行
                        var lines = $("#section" + nextIndex + " .pic-des-" + currPicHeightType + "-des p").length + $("#section" + nextIndex + " .pic-des-" + currPicHeightType + "-des div").length;
                        var perpHeight = leftBtmHeight / lines;
                        var tmpSpan = $('<span style="font-size:xx-large;margin-top:0px;margin-bottom:0px;padding:0px;font-family:微软雅黑;visibility:hidden;">汉字</span>').appendTo($("#section" + nextIndex));
                        var txtHeight = tmpSpan.height();
                        tmpSpan.remove();
                        if($(document).width() > 1100){//针对电脑
                            $("#section" + nextIndex + " .pic-des-" + currPicHeightType + "-des p").css("font-size", "large");
                            $("#section" + nextIndex + " .pic-des-" + currPicHeightType + "-des p").css("height", 0.28 * txtHeight + "px");
                            $("#section" + nextIndex + " .pic-des-" + currPicHeightType + "-des div").css("margin", "0px 0px " + 0.25 * txtHeight + "px 0px" );
                            $("#section" + nextIndex + " .pic-des-" + currPicHeightType + "-des p").css("line-height",  0.28 * txtHeight + "px");
                        }else{
                            $("#section" + nextIndex + " .pic-des-" + currPicHeightType + "-des p").css("font-size", "xx-large");
                        $("#section" + nextIndex + " .pic-des-" + currPicHeightType + "-des p").css("height", 0.47 * txtHeight + "px");
                        $("#section" + nextIndex + " .pic-des-" + currPicHeightType + "-des div").css("margin", 0.25 * txtHeight + "px 0px");
                        $("#section" + nextIndex + " .pic-des-" + currPicHeightType + "-des p").css("line-height",  0.47 * txtHeight + "px");
                        }
                    }
                    //尾页
                    else if($("#section" + nextIndex + " .end-sec").length != 0){
                        //设置行高使得居中显示
                        var headheight = $($("#section" + nextIndex + " .end-head")[0]).height();
                        $("#section" + nextIndex + " .end-head,#section" + nextIndex + " .end-content").css("line-height", headheight + "px");
                            var moreheight = $($("#section" + nextIndex + " .end-more")[0]).height();
                        $("#section" + nextIndex + " .end-more").css("line-height", moreheight + "px");
                    }
                    //尾页删除箭头
                    if($("#section" + nextIndex + " .end-sec").length != 0){
                        $("#pageArrow").hide();
                    }
                    else{
                        $("#pageArrow").show();
                    }
                    //删除上一页的动画并隐藏所有动画要素

                    $("#section" + index + " .anim1,#section" + index + " .anim2,#section" + index + " .anim3,#section" + index + " .anim4").css("visibility", "hidden").removeClass("animated");
                    $("#section" + nextIndex + " .anim1,#section" + nextIndex + " .anim2,#section" + nextIndex + " .anim3,#section" + nextIndex + " .anim4").css("visibility", "visible");
                    //分组动画时延
                    var aniIndex = 1;
                    var anistrs = $("#section" + nextIndex + " .marksectype").attr("anitype");
                    var aniArray = (!anistrs)? [] : anistrs.split(" ");
                    while($("#section" + nextIndex + " .anim" + aniIndex).length != 0){//该section下面有动画
                        //设置动画延迟并添加动画完成事件--移除动画类并设置为隐藏
                    	$("#section" + nextIndex + " .anim" + aniIndex).css("-webkit-animation-delay", (aniIndex * 0.8) + "s");
                        $("#section" + nextIndex + " .anim" + aniIndex).each(function (elemIndex, elem){
                            elem.addEventListener("webkitAnimationEnd", function(){
                                $(elem).removeClass("animated");
                                $(elem).removeClass($(elem).attr("curranitype"));
                            });
                        });

                        $("#section" + nextIndex + " .anim" + aniIndex).addClass(aniArray[aniIndex - 1]);
                        $("#section" + nextIndex + " .anim" + aniIndex).attr("curranitype", aniArray[aniIndex - 1]);
                        if(!$("#section" + nextIndex + " .anim" + aniIndex).hasClass("animated")){
                            $("#section" + nextIndex + " .anim" + aniIndex).addClass("animated");
                        }
                        aniIndex++;
                    }
				}
			});

    if($(document).width()> $(document).height()){
        $("#fullpage").width($(document).height() * 3 /4 );
    }
    loadFirstPageStructure();

        };
